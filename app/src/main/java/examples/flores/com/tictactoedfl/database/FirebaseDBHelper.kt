package examples.flores.com.tictactoedfl.database

import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener

class FirebaseDBHelper {


    /**
     * Function used to insert score into the Firebase database
     */
    companion object {
        val database = FirebaseDatabase.getInstance()
        val reference = database.getReference("score")
        fun insertData(type: Int, value: Int) {
            var key = ""
            var values = mutableMapOf<String, Int>()
            when (type) {
                1 -> {
                    key = "Player 1 Wins"
                }
                2 -> {
                    key = "Player 2 Wins"
                }
                3 -> {
                    key = "Draws"
                }
            }
            values.put(key, value)
            var r = reference.child(key)
            r.setValue(value)

        }

        /**
         * function used to delete all the data in the Firebase Database
         */
        fun deleteAll() {
            reference.setValue(null)
        }

        fun readData() {
            reference.addValueEventListener(object : ValueEventListener {
                override fun onDataChange(p0: DataSnapshot) {
                    println("datasnapshot $p0")
                }

                override fun onCancelled(error: DatabaseError) {
                    println("error ${error.message}")
                }
            })
        }
    }


}