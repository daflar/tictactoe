package examples.flores.com.tictactoedfl

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Button
import android.widget.Toast
import com.google.firebase.database.FirebaseDatabase
import examples.flores.com.tictactoedfl.database.FirebaseDBHelper
import kotlinx.android.synthetic.main.activity_main.*

class TicTacToeMain : AppCompatActivity(), View.OnClickListener {

    private val buttons = Array<Array<Button?>>(3) { arrayOfNulls(3) }
    private var playerOne = true
    private var counter = 0;
    private var victoriesCounterP1 = 0;
    private var victoriesCounterP2 = 0;
    private var drawCounter = 0;
    private val database = FirebaseDatabase.getInstance()
    private val reference = database.getReference("score")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        for (i in 0..2) {
            for (j in 0..2) {
                val buttonID = "btn_$i$j"
                val resID = resources.getIdentifier(buttonID, "id", packageName)
                buttons[i][j] = findViewById<View>(resID) as Button
                buttons[i][j]?.setOnClickListener(this)
            }
        }
        restart(true)
        this.restart_btn.setOnClickListener { restart(true) }

    }


    override fun onClick(p0: View?) {
        println((p0 as Button).text.toString())

        if ((p0).text.toString() != "") {
            return
        }
        if (playerOne) {
            (p0).text = getString(R.string.x)
            player_turn_tv.text = getString(R.string.turn_p_two)
        } else {
            (p0).text = getString(R.string.o)
            player_turn_tv.text = getString(R.string.turn_p_one)
        }

        counter++

        if (checkGameFinished()) {
            if (playerOne) {
                showWinnerPlayer(1)
            } else {
                showWinnerPlayer(2)
            }
        } else if (counter == 9) {
            showWinnerPlayer(3)
        } else {
            playerOne = !playerOne
        }
    }

    /**
     * Display the data to show wich player won, also insert the score into Firebase Database
     */
    private fun showWinnerPlayer(b: Int) {
        var winner = ""
        when (b) {
            1 -> {
                winner = String.format(getString(R.string.winner), "1")
                victoriesCounterP1++
                FirebaseDBHelper.insertData(1, victoriesCounterP1)
                player_one.text = String.format(getString(R.string.player_one), victoriesCounterP1.toString())
            }
            2 -> {
                winner = String.format(getString(R.string.winner), "2")
                victoriesCounterP2++
                FirebaseDBHelper.insertData(2, victoriesCounterP2)
                player_two.text = String.format(getString(R.string.player_two), victoriesCounterP2.toString())
            }
            3 -> {
                winner = getString(R.string.draw)
                drawCounter++
                FirebaseDBHelper.insertData(3, drawCounter)
                draws.text = String.format(getString(R.string.draws), drawCounter.toString())
            }
        }
        Toast.makeText(this, winner, Toast.LENGTH_SHORT).show()
        FirebaseDBHelper.readData()
        restart(false)
    }

    /**
     * Function used to delete all the variables and if param deleteAll it's true then the data saved in Firebase database will be removed
     *
     */
    private fun restart(deleteAll: Boolean) {
        for (i in 0..2) {
            for (j in 0..2) {
                buttons[i][j]?.text = ""
            }
        }
        counter = 0
        playerOne = true
        if (deleteAll) {
            victoriesCounterP2 = 0
            victoriesCounterP1 = 0
            drawCounter = 0
            FirebaseDBHelper.deleteAll()
            player_one.text = String.format(getString(R.string.player_one), victoriesCounterP1.toString())
            player_two.text = String.format(getString(R.string.player_two), victoriesCounterP2.toString())
            draws.text = String.format(getString(R.string.draws), drawCounter.toString())
        }
        player_turn_tv.text = getString(R.string.turn_p_one)
    }

    /**
     * Function used to check if the game has finished checking button by button to know any posible combination
     */
    private fun checkGameFinished(): Boolean {
        var field = Array(3) { Array(3) { "" } }
        for (i in 0..2) {
            for (j in 0..2) {
                field[i][j] = buttons[i][j]?.text.toString()
            }
        }

        for (i in 0..2) {
            if (field[i][0] == field[i][1]
                && field[i][0] == field[i][2]
                && field[i][0] != ""
            ) {
                return true
            }
        }

        for (i in 0..2) {
            if (field[0][i] == field[1][i]
                && field[0][i] == field[2][i]
                && field[0][i] != ""
            ) {
                return true
            }
        }

        if (field[0][0] == field[1][1]
            && field[0][0] == field[2][2]
            && field[0][0] != ""
        ) {
            return true
        }

        if (field[0][2] == field[1][1] && field[0][2] == field[2][0] && field[0][2] != "") {
            return true
        }

        return false

    }

}
