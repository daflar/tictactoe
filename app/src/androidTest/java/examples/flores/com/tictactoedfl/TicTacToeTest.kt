package examples.flores.com.tictactoedfl

import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class TicTacToeTest {

    @JvmField
    @Rule
    val testRule = ActivityTestRule<TicTacToeMain>(TicTacToeMain::class.java)

    private val ticTacToeScreen = TicTacToeScreen()
    @Test
    fun initialViewsDisplayedProperly() {
        ticTacToeScreen {
            restartBtn { isDisplayed() }
            playerTurnTv {
                isDisplayed()
                hasText("Player 1")
            }
            playerOneTv {
                hasText("P1 wins: 0")
            }
            playerTwoTv {
                hasText("P2 wins: 0")
            }
            drawsTv {
                hasText("Draws: 0")
            }
        }
    }

    @Test
    fun displayPlayerOneWinner() {
        ticTacToeScreen {
            playerTurnTv {
                isDisplayed()
                hasText("Player 1")
            }
            btn00 { click() } //x
            playerTurnTv {
                hasText("Player 2")
            }
            btn01 { click() } //o
            playerTurnTv {
                isDisplayed()
                hasText("Player 1")
            }
            btn11 { click() } //x
            playerTurnTv {
                isDisplayed()
                hasText("Player 2")
            }
            btn02 { click() }//o
            playerTurnTv {
                isDisplayed()
                hasText("Player 1")
            }
            btn22 { click() }//x

            playerTurnTv {
                isDisplayed()
                hasText("Player 1")
            }
            playerOneTv {
                isDisplayed()
                hasText("P1 wins: 1")
            }
            playerTwoTv {
                hasText("P2 wins: 0")
            }
            drawsTv {
                hasText("Draws: 0")
            }

        }
    }

    @Test
    fun displayPlayerTwoWinner() {
        ticTacToeScreen {
            playerTurnTv {
                isDisplayed()
                hasText("Player 1")
            }
            btn00 { click() } //x
            playerTurnTv {
                isDisplayed()
                hasText("Player 2")
            }
            btn01 { click() } //o
            playerTurnTv {
                isDisplayed()
                hasText("Player 1")
            }
            btn12 { click() } //x
            playerTurnTv {
                isDisplayed()
                hasText("Player 2")
            }
            btn11 { click() }//o
            playerTurnTv {
                isDisplayed()
                hasText("Player 1")
            }
            btn22 { click() }//x

            playerTurnTv {
                isDisplayed()
                hasText("Player 2")
            }

            btn21 { click() }//o
            playerTwoTv {
                hasText("P2 wins: 1")
            }
            playerOneTv {
                hasText("P1 wins: 0")
            }
            drawsTv {
                hasText("Draws: 0")
            }

        }
    }

    @Test
    fun displayPlayerDraw() {
        ticTacToeScreen {
            playerTurnTv {
                isDisplayed()
                hasText("Player 1")
            }
            btn00 { click() } //x
            playerTurnTv {
                isDisplayed()
                hasText("Player 2")
            }
            btn01 { click() } //o
            playerTurnTv {
                isDisplayed()
                hasText("Player 1")
            }
            btn10 { click() } //x
            playerTurnTv {
                isDisplayed()
                hasText("Player 2")
            }
            btn20 { click() }//o
            playerTurnTv {
                isDisplayed()
                hasText("Player 1")
            }
            btn02 { click() }//x

            playerTurnTv {
                isDisplayed()
                hasText("Player 2")
            }

            btn11 { click() }//o
            playerTurnTv {
                isDisplayed()
                hasText("Player 1")
            }
            btn21 { click() }//x
            playerTurnTv {
                isDisplayed()
                hasText("Player 2")
            }
            btn22 { click() }//o
            playerTurnTv {
                isDisplayed()
                hasText("Player 1")
            }
            btn12 { click() }//x
            playerTurnTv {
                isDisplayed()
                hasText("Player 1")
            }
            playerOneTv {
                hasText("P1 wins: 0")
            }
            playerTwoTv {
                hasText("P2 wins: 0")
            }
            drawsTv {
                hasText("Draws: 1")
            }

        }
    }

    @Test
    fun buttonRestart() {
        ticTacToeScreen {
            playerTurnTv {
                isDisplayed()
                hasText("Player 1")
            }
            btn00 { click() } //x
            playerTurnTv {
                isDisplayed()
                hasText("Player 2")
            }
            btn01 { click() } //o
            playerTurnTv {
                isDisplayed()
                hasText("Player 1")
            }
            restartBtn { click() }
            playerTurnTv {
                isDisplayed()
                hasText("Player 1")
            }
            playerOneTv {
                hasText("P1 wins: 0")
            }
            playerTwoTv {
                hasText("P2 wins: 0")
            }
            drawsTv {
                hasText("Draws: 0")
            }
            btn00 {
                hasText("")
            }

            btn01{
                hasText("")
            }
        }
    }

}