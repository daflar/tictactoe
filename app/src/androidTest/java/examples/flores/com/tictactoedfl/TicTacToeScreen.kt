package examples.flores.com.tictactoedfl

import com.agoda.kakao.KButton
import com.agoda.kakao.KTextView
import com.agoda.kakao.Screen

class TicTacToeScreen : Screen<TicTacToeScreen>() {

    val playerTurnTv : KTextView = KTextView{withId(R.id.player_turn_tv)}
    val playerOneTv : KTextView = KTextView{withId(R.id.player_one)}
    val playerTwoTv : KTextView = KTextView{withId(R.id.player_two)}
    val drawsTv : KTextView = KTextView{withId(R.id.draws)}
    val restartBtn : KButton = KButton{withId(R.id.restart_btn)}
    val btn00 : KButton = KButton{withId(R.id.btn_00)}
    val btn01 : KButton = KButton{withId(R.id.btn_01)}
    val btn02 : KButton = KButton{withId(R.id.btn_02)}
    val btn10 : KButton = KButton{withId(R.id.btn_10)}
    val btn11 : KButton = KButton{withId(R.id.btn_11)}
    val btn12 : KButton = KButton{withId(R.id.btn_12)}
    val btn20 : KButton = KButton{withId(R.id.btn_20)}
    val btn21 : KButton = KButton{withId(R.id.btn_21)}
    val btn22 : KButton = KButton{withId(R.id.btn_22)}

}